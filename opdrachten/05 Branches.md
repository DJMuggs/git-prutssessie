# Branches
Een van de mooiste dingen aan git (en andere vcs'en) is het principe van branching.
Het principe is redelijk makkelijk uit te leggen. Stel er is maar een branch (de master branch).
En 1 ontwikkelaar maakt commits op deze branch. Dan ontstaat een mooie sliert van commits, van het begin de repo, tot HEAD van de master branch.
En met elke commit wordt de HEAD van master 1 plaats opgeschoven (naar de dan weer laatste commit).
Stel nu dat er twee ontwikkelaars gaan ontwikkelen. Tenzij ze allebei om de beurt voor iedere commit een pull en een push doen,
zullen de lokale branches soms een beetje afwijken. Maar uiteindelijk trekt git bij iedere `git pull` de lokale repository weer recht
met de remote master branch. Het uiteindelijke resultaat is nog steeds een lange sliert van commits, van begin tot HEAD.
Maar stel je nu eens voor dat een ontwikkelaar big fixes gaat doen, terwijl de andere ontwikkelaar een nieuwe functionalitiet gaat bouwen.
De bug fixes moeten zo snel als mogelijk naar de master branch. Maar de nieuwe functionaliteit moet pas naar de master branch
als hij stabiel is. En dit is dus waar branching zo praktisch is. Men kan bijvoorbeeld een develop branch ernaast maken.
De bigfixes kunnen nog steeds direct naar master, terwijl de nieuwe functionaliteiten naar develop kunnen.
Beide ontwikkelaars kunnen zelfs afwisselend werken aan bugfixes en/of nieuwe functionaliteit, tot tussen barcnhes te wisselen.
Bugfixes (master) kunnen keer op keer in develop gemerged worden zodat develop niet teveel afwijkt en zodat nieuwe ontwikkelingen
kunnen worden gebaseerd op werkende code. En als de nieuwe feature klaar is (en stabiel), dan kan hij in master gemerged worden.

Kortom, branching (afsplitsen), doe je om verschillende stromen commits te scheiden.

Voor branching is een mooie theorie opgesteld over wat voor verschillende soorten branches je zou kunnen maken en
wanneer je dan afsplitst (brancht) en weer samenvoegt (merged). De theorie heet git-flow en geeft een goede weergave
van hoe een enorm ingewikkeld ontwikkelproces op een complex project (zoals de linux kernel) met 100-en ontwikkelaars
nog steeds werkbaar kan zijn. Kijk voor meer informatie op:
* Git flow: https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
* Development model: http://nvie.com/posts/a-successful-git-branching-model/

Verder is er een website die grafisch weergeeft wat het resultaat is van branching merging, comitting, etc. Kijk voor meer informatie op:
* Learn git branching: https://learngitbranching.js.org/

# Oefening 5
* Controlleer de huidige status mbt branches:
```
git branch
```
* Maak een aparte branch genaamd naar jezelf (je eigen ontwikkel branch). Controller opnieuw de status van branches.
```
git branch [MijnNaam]
git branch
```
* maak een commit op de huidige branch (je bent nog niet over geschakeld naar de nieuwe branch).
  Controlleer de laatste commits.
```
date >> deelnemers/log.txt
git commit 'deelnemers/log.txt' -m "datum toegevoegd aan deelnemers/log.txt"
git log | head
```
* Schakel naar de nieuwe branch en controller op. Merk op dat je laatste commit op master er niet bij staat.
```
git checkout [MijnNaam]
git log | head
```
* maak op de nieuwe branch een aantal extra commits en controlleer `git log`
```
for ((i=1; i<5; i++)); do 
  echo "$(date): regel $i ingevoegd" >> deelnemers/log.txt ; git commit 'deelnemers/log.txt' -m "Datum $i toegevoegd aan deelnemers/log.txt"; sleep 2; done
done
git log
```
* Voeg de commit van de master branch toe aan de nieuwe branch en controller `git log`
```
git merge master
git log
```
* selecteer een willekeurige eerdere commit en maak hiervan een nieuwe branch (je schakelt nu wel naar de nieuwe branch)
```
git checkout -b nieuwe_branch [commit]
```
* Selecteer opnieuw een eerdere commit (van deze branch) en rol terug naar deze eerdere commit. De aanpassingen van de commits komen in je huidige workdir terecht.
```
git reset [commit]
```
* maak opnieuw een aanpassing 
```
date >> deelnemers/log.txt
git commit 'deelnemers/log.txt' -m "datum toegevoegd aan deelnemers/log.txt"
```
* doe een rebase van deze branch op basis van je eigen branch (git zoekt een 'common ancestor' commit, voert daarop eerst de commits uit de andere branch door en daarna alle commits uit deze branch)
```
git rebase [MijnNaam]
git log
```
* ruim alles weer op
```
git checkout master
git branch -D [MijnNaam] nieuwe_branch
```
