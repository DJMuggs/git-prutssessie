# Repo managers
Je kunt gewoon een remote maken middels `git init -b`. En dan toegang tot de branch regelen middels ssh, gitweb en/of het git protocol.
Maar de mogelijkheden van een dergelijke remote zijn wel beperkt en tegenwoordig zijn er veel alternatieven die een enorme meerwaarde kunnen bieden.
Een aantal voorbeelden zijn:
* gitlab
  * Omschrijft zichzelf als: online Git repository manager with a wiki, issue tracking, CI and CD
  * https://about.gitlab.com/2015/05/18/simple-words-for-a-gitlab-newbie/
  * Lokaal installerbaar installeerbaar
  * Uitgebreide webinterface, vergelijkbare functionaliteiten met github
  * Integreerbaar met andere interne systemen, zoals een CI/CD pipeline
  * markdown met .md bestanden
* github
  * Omschrijft zichzelf als: a web-based hosting service for version control using git
  * https://en.wikipedia.org/wiki/GitHub
  * Vrijelijk bruikbaar, op internet
  * Voor een kleine bijdrage heb je vele extra opties, zoals 'private repos'
  * Ik vind de issues tracker heel praktisch
  * Versies middels tags
  * markdown met .md bestanden
* gerrit
  * web-based code review tool built on top of the git version control system
  * https://review.openstack.org/Documentation/intro-quick.html
  * Maak van de commits changes, compleet met reviews en mogelijkheid om per commit te bepalen wanneer deze gemerged wordt
* gitweb
  * Git web interface (web frontend to Git repositories)
  * https://git-scm.com/docs/gitweb
  * standaard meegeleverd met git
  * zeer praktisch om een git repo middels http(s) te ontsluiten. Verder zeer beperkt in mogelijkheden

# Oefening 6
* Merge de aanpassingen van je persoonlijke repo (Oefening 5) in de centrae repo middels een merge request
  * ga naar de url van je persoonlijke repo
  * klik op de + (tussen het wolkje en de bel)
  * selecteer 'new merge request'
  * selecteer de source branch (develop) en merge naar prutssessie/git_20180321 op branch master
  * klik op 'compare branches and continue'
  * geef een titel en beschrijving
  * laat de rest onveranderd en klik op submit merge request
  * Je wijziging staat bij de shared repo erbij (links vind je onder 'Issues' een menuitem 'Merge Requests') en kan worden doorgevoerd middels de merge button
  * Eventueel kun je op deze manier de master branch dicht zetten voor pushing, zodat hij alleen nog middels merge requests kan worden aangevuld.
    ('Settings' / 'Repository' / 'Protected Branches' / 'Allowed to push')
