# Merge and conflicts
Als je aanpassingen gedaan hebt, dan is het heel belangrijk om die aanpassingen te delen:
* met je collega's, zodat die erop kunnen verder bouwen
* met het buildproces, de working version, etc.

Typisch kun je stellen dat je aanpassingen pas actief zijn als ze in de master branch aanwezig zijn.
Hier is veel over te vertellen. Bijvoorbeeld over branching. En over Remotes. Maar dat zijn andere hoofdstukken.
In dit hoofdstuk is het uitgangspunt heel simpel, je wilt een wijziging van je workdir direct naar origin/master krijgen.
Origin is 

## Happy flow
Eigenlijk zijn er twee flows te beschrijven:
* De happy flow, waarin alles loopt als een zonnetje
* Een conflict

Een paar jaar geleden ben ik erachter gekomen dat de volgende happy flow eigenlijk heel erg goed werkt:
1. Voor je lokaal gaat aanpassen haal je de laatste versie van de remote binnen met `git pull`
  * Des te minder git hoeft te mergen, des te groter de kans dat de happy flow opgaat
2. Je doet je aanpassingen in je workdir (open wat bestanden, pas ze aan en sla ze op).
3. Test je aanpassingen (je wilt natuurlijk wel dat je zeker bent dat wat je doorvoert ook goed is).
  * Stap 2 en 3 kun je itereren
4. Je kiest je bestanden voor staging met het commando `git add [bestand1] (bestand2 ...)`
5. Je maakt je commit (gebaseerd op de gestagede bestanden) met het commando `git commit`
6. Je controleert nogmaal op eventuele last minute aanpassingen van anderen middel `git pull`
  * In de happy flow zal git je aanpassingen altijd kunnen mergen met de eventuele last minute aanpassingen.
7. Je stuurt je aanpassingen naar origin met `git push`

Doe dit met kleine iteraties (hak je aanpassingen op in kleine stapjes, misschien tot een per uur, of zo) om 
de kansen op conflicten nog verder te minimaliseren.

## conflicts
De happy flow gaat voor simpelere ontwikkel processen vaak goed en het is prima om op deze manier als team (2 - 10 personen)
aan code te werken (bijvoorbeeld een script repo).
Eigenlijk gaat de happy flow pas fout als git code moet samenvoegen op dezelfde regel. Bijvoorbeeld:
* Last minute wijzigingen veranderen een regel die jij ook aanpast en beide is anders.
* Last minute wijzigingen veranderen een regel die jij weg gooit (of anders)
En de kans hierop hangt samen met
* activiteiten van anderen tussen stap 1 en stap 6 (meer activiteit, grotere kans).
* aanregels code (meer regels, kleinere kans)

Maar we praten over kans en er kan altijd een conflict optreden. In dat geval moet het conflict (met de hand) worden opgelost.
Eigenlijk is dit heel simpel. Bij stap 6 zal git een merge conflict rapporteren op een of meerdere bestanden en wat je moet doen.
Het komt neer op onderstaande:
Open de bestanden en zoek naar regels met heel veel '<' characters of '>' characters.
git heeft hier een conflict weer, als volgt:
```
<<<<<<< HEAD
[Code origin regel 1]
[Code origin evt regel 2, regel 3, etc.]
=======
[Code jouw commit regel 1]
[Code jouw commit evt regel 2, regel 3, etc.]
>>>>>>> repo1
```

Vervang dit conflict block door wat er echt moet staan. Bijvoorbeeld:
```
[Code origin regel 1]
[Code jouw commit regel 1]
[Code origin evt regel 2, regel 3, etc.]
[Code jouw commit evt regel 2, regel 3, etc.]
```

Daarna kun je de uiteindelijk gemergde versie committen middels `git add [file1 ([file 2] ...) ; git rebase --continue`.

# Oefening 2
## Oefening 2a: Happy flow (add, commit, pull, push)
1. Haal de laatste versie van de remote binnen met `git pull`
2. Pas je eigen bestand aan
3. Je kiest je bestand voor staging
4. Maak je commit
6. Een voor een:
 * Controleer op eventuele last minute aanpassingen
 * Stuur je aanpassingen naar origin

```
cd ~/gitlab_conclusion/git_20180321
git pull
echo "Telefoonnummer: 06-82521560" >> deelnemers/SMannem.txt
git add deelnemers/SMannem.txt
git commit -m 'Telefoonnummer toegevoegd voor SMannem'
#Wacht op je beurt en dan
git pull
git push
```

## Oefening 2b: Conflicts (add, commit, pull, fix, add, commit, push)
1. Haal de laatste versie van de remote binnen met `git pull`
2. Pas allemaal hetzelfde bestand aan
3. Kies je bestand voor staging
4. Maak je commit
6. Een voor een:
 * Controleer op eventuele last minute aanpassingen
 * Stuur je aanpassingen naar origin

```
cd ~/gitlab_conclusion/git_20180321
git pull
echo "$(date): S. Mannem toegevoegd" >> deelnemers/log.txt
git add deelnemers/log.txt
git commit -m 'Log bijgewerkt voor toevoegen van SMannem'
#Wacht op je beurt en dan
git pull
#Los het conflict op in log.txt en dan
git add deelnemers/log.txt
git rebase --continue
git push
```
