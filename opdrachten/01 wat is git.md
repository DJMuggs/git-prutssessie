# Versiebeheer
Versiebeheer gaat over het bijhouden van versies.

Je kunt het zelf doen. Bijvoorbeeld met word documenten, die je op slaat met een timestamp/versienummer in de naam.

Het lastigere hiervan is wel dat het verschil tussen deze en de vorige versie niet echt inzichtelijk is.

Dat kun je fixen, bijvoorbeeld door in word 'wijzigingen bijhouden' aan te zetten en elke nieuwe versie de wijzigingen 
geschiedenis leeg te maken, maar dat is een handmatig proces en dat is niet super.

## Met meerderen tegelijk
Als het proces die de aanpassingen doet wordt uitgevoerd door meer mensen, dan komen een extra aantal interessante dingen erbij kijken.
* Metadata wordt ineens heel belangrijk. Wie heeft deze aanpassing gedaan. Wanneer. En Waarom.
* Maar ook een 'schaduw proces', zodat sommigen kunnen werken aan de volgende 'grote versie', terwijl anderen kleine aanpassingen doen.
En dan die dingen later samen voegen.

## Bij broncode
Software broncode heeft een bepaalde structuur. Het is platte text, (bijna altijd) opgedeeld in meerdere bestanden en er zijn meerdere 
soorten bestanden, zoals bijvoorbeeld de echte broncode, header file, maar ook textfiles, zoals README.md, LICENSE.txt, etc.

Het is dus goed te doen om aanpassingen op broncode te analyseren. En dat is eigenlijk wat GIT doet.

## Hoe
Eigenlijk maar git een hash van iedere regel broncode en houd die bij elkaar in een database.
Vervolgens kun je aanpassingen doen in de broncode en dan kan git erg makkelijk aangeven welke regels veranders zijn
* erbij zijn gekomen
* weg zijn gevallen
* veranderd (eigenlijk is dat dus beide voorgaande)

Die verschillen kan git dan groeperen (git add), en als 1 logisch geheel opslaan in de database (een commit).
Door vervolgens op een situatie (commit) alle veranderingen door te voeren kan git e nieuwe situatie herbouwen (roll forward / rollback).

Dit werkt prima. Bij broncode. En git is prima inzetbaar voor andere dingen. Maar ook slecht inzetbaar voor een aantal andere zaken.

Git is niet krachtig bij hele grote bestanden. En ook niet bij binaire bestanden.

# Opdracht 1
Kan zijn dat je onderweg tegen extra meldingen aanloopt, zoals het feit dat je in git je naam en email moet configureren.

Gewoon doen wat er staat. En kom je er niet uit, vraag dan hulp.
* Installeer git (afhankelijk van je distributie). Voorbeelden:
  * `sudo dnf install git-all`
  * `sudo apt-get install git-all`

* Clone de prutssessie repository naar je locale PC:
```
mkdir -p ~/gitlab_conclusion
cd ~/gitlab_conclusion
git clone git@xforce.tilaa.cloud:prutssessie/git_20180321.git
cd git_20180321
```
* Maak een folder 'deelnemers' (als die niet al bestaat) en zet daarin een bestandje met wat info over jezelf. Voorbeeld:
```
cd ~/gitlab_conclusion/git_20180321
mkdir deelnemers
echo 'Sebastiaan Mannem, Linux specialist.
Email: sebastiaan.mannem@conclusionxforce.nl' > deelnemers/SMannem.txt
```
* Commit het bestand naar git en push het naar de remote
```
git add deelnemers/SMannem.txt
git commit -m 'deelnemersbestand voor SMannem'
git push
```
