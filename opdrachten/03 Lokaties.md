# Info
Een van de mooie dingen van git, is dat, terwijl je eigenlijk bezig bent om controle te krijgen en houden 
over het ontwikkelproces, je ondertussen ook enorm veel informatie genereert.

Een mooi voorbeeld van die informatie is te vinden als je googled op 'visualisation git kernel'.
O.a. onderstaande filmpje staat in de lijst met resultaten:
* Visualisation: https://www.youtube.com/watch?v=5iFnzr73XXk
Het filmpje geeft een visualisatie van de ontwikkelingen van de kernel en is tot stand gekomen door de git repo 
van de linux kernel door een visualisatie tool te halen.

Naast mooie filmpjes is die informatie ook enorm waardevol, omdat je er enorm goed terug kunt vinden waar bepaalde
onderdelen va je code nu precies vandaan komen. Maar hoe krijg je nu toegang tot die informatie.

## Waar staat mijn wijziging
Een code wijziging kan zich op verschillende plaatsen in het proces bevinden:
* in de workdir.
  * Dit betekent dat jij de aanpassing hebt doorgevoerd en er verder nog niets mee is gedaan
* in de stash locatie.
  * Dit betekent dat je de aanpassing tijdelijk hebt weg gezet met `git stash`.
* in de staging locatie. WIjzigingen die zich hierin bevinden gaan mee met een volgende commit
* in een commit. Wijzigingen zijn middels `git commit` vanuit de staging locatie in een commit onder gebracht
* Op de remote. De commit is middels `git push` naar de remote gestuurd.

Eigenlijk werkt git voornamelijk lokaal. Dat betekent dat je met git:
* informatie kunt opvragen over je workdir, stash, staging, en lokale commits
* na een `git pull` zijn alle commits van de remote (van de huidige branch) lokaal gehaald en kun je daar ook informatie over krijgen.

## Hoe komt mijn wijziging daar
* Workdir
  * Aanpassing in de workdir is makkelijk. open het bestand, pas het aan en sla de gewijzigde versie op
  * Aanpassing in de workdir ongedaan maken doe je door middel van `git checkout -- [file] ([file2] ...)`
* Stash (tijdelijk even parkeren)
  * met het commando `git stash` wordt de huidige state (en alle aanpassingen uit de workdir) op de stash geplaatst
  * met het commando `git stash pop` wordt de laatste git stash weer terug geplaatst op de workdir
  * let op dat `git stash` niet de bestanden, maar de wijzigingen opslaat (net als een commit alleein de wijzigingen bevat.)
    En dat `git stash pop` een merge de wijzigingen weer terug doorvoert op de workdir.
    Er kan dus (net zo goed) merge conflicts ontstaan. En wijzigingen in de workdir worden niet terug gedraaid door een gt stash pop.
  * Nieuwe bestanden worden niet in de stash geplaatst en/of terug gezet.
  * met het commando `git stash list` kun je de stash items bekijken
* Staging (klaar zetten voor de volgende commit)
  * met het commando `git add [file]` wordt de aanpassingen op de file in de stage area geplaatst
  * met het commando `git reset HEAD [file]` wordt de aanpassingen uit de stage area terug geplaatst in de workdir
* Commit
  * met het commando `git commit` worden alle wijzigingen uit de staging area in een commit samen gebracht.
  * met het commando `git reset [previous commit]` wordt de commit terug gedraaid en aanpassingen op de commit weer doorgevoerd op de workdir
  * met het commando `git reset --hard [previous commit] wordt de commit terug gedraaid, zonder aanpassen van de workdir`
* Remote
  * met het commando `git push` wordt de commit(s) naar de remote gestuurd
  * met het commando `git pull` wordt de aanpassingen van anderen naar de lokale git repo gehaald en de huidige branch bijgewerkt naar de laatste commit

# Oefening 3
* Pas een bestand aan in de workdir en zet een nieuw bestand erbij. Controleer git status
```
FNAME=$(date +%s)
echo $FNAME > "deelnemers/${FNAME}.txt"
date >> "deelnemers/log.txt"
git status
```
* Controleer de stash, stash je aanpassingen en check je stash opnieuw en git status opnieuw
```
git stash list
git stash
git stash list
git status
```
* Plaats de aanpassingen terug van de stash en check git status opnieuw
```
git stash pop
git status
```
* Plaats de aanpassingen in de staging area en check git status opnieuw
```
git add deelnemers
git status
```
* Reset de aanpassingen in de staging area in check git status
```
git reset HEAD "deelnemers/log.txt" "deelnemers/${FNAME}.txt"
git status
```
* Reset de aanpassingen in de workdir en check git status opnieuw
```
git checkout -- "deelnemers/log.txt" "deelnemers/${FNAME}.txt"
git status
```

Als het goed is is de workdir, staging area en stash weer teug bij af.
