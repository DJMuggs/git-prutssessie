# Inhoudsopgave

# Inleiding
* Waarom git, wat is git, history, etc.
* Oefening 1: Intro (clone, add, commit, push)

# Merge and conflicts
* Oefening 2a: Happy flow (add, commit, pull, push)
* Oefening 2b: Conflicts (add, commit, pull, fix, add, commit, push)

# Locaties (Areas)
* Waar staat mijn wijziging
* Oefening 3: bestanden verplaatsen tussen verschillende locaties (git stash, git add, git commit, git reset HEAD en git checkout --)

# Info
* Waar haal je info vandaan
* Oefening 4: Info opvragen (status, log, blame, show, stash list)

# Branches
* Wat is een branch
* Oefening 5: 
  * Maken en aanpassen van een branch (branch, merge, rebase, reset, checkout)
  * Switchen tussen branches
  * Branches naar remotes sturen en ophalen

# Remotes
* Wat is een remote
* Oefening 6: Config van remotes (remote)

# Repo managers
* gitlab
  * online Git repository manager with a wiki, issue tracking, CI and CD
  * https://about.gitlab.com/2015/05/18/simple-words-for-a-gitlab-newbie/
* github
  * a web-based hosting service for version control using git
  * https://en.wikipedia.org/wiki/GitHub
* gerrit
  * web-based code review tool built on top of the git version control system
  * https://review.openstack.org/Documentation/intro-quick.html
* gitweb
  * Git web interface (web frontend to Git repositories)
  * https://git-scm.com/docs/gitweb
* Oefening 6: Merge request

# Extra
* Learn git branching: https://learngitbranching.js.org/
* Visualisation: https://www.youtube.com/watch?v=5iFnzr73XXk
* Git flow: https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
* Development model: http://nvie.com/posts/a-successful-git-branching-model/
